import Vue from 'vue'
import Vuex from 'vuex'
import App from './App'
import VueRouter from "vue-router"
import VueResource from "vue-resource";

Vue.config.debug = true;


Vue.use(VueRouter);
Vue.use(VueResource);
Vue.use(Vuex);
const home = resolve => require(['./components/home'], resolve)
const gifts = resolve => require(['./components/open-gifts'], resolve)
const animation = resolve => require(['./components/animation'], resolve)
const personalize = resolve => require(['./components/personalize'], resolve)
const preview = resolve => require(['./components/preview'], resolve)
const share = resolve => require(['./components/share'], resolve)
//
const router = new VueRouter({
  // mode: 'history',
  base: __dirname,
  routes: [
    {
      path: '/home',
      component: home
    },
    {
      path: '/gifts',//打开礼物
      component: gifts
    },
    {
      path: '/',//主页
      component: home
    },
    {
      path: '/animation',
      component: animation
    },
    {
      path: '/animation/:id',
      component: animation
    },
    {
      path: '/personalize',
      component: personalize
    },
    {
      path: '/preview',
      component: preview
    },
    {
      path: '/share',
      component: share
    }
  ]
})

//vuex
const store = new Vuex.Store({
  state: {
    date: '',
    message:'',
    imgType:'',
    images:[]
  },
  mutations: {
    userChoice (state,payload) {
      state.date = payload.date;
      state.message = payload.message;
      state.imgType = payload.imgType;
    },
    addImage (state,payload){
      state.images = payload.imgArray
    }
  }
})
/* eslint-disable no-new */
new Vue({
  el: '#app',
  store:store,
  router: router,
  template: '<App/>',
  components: { App }
})
