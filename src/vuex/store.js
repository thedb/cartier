import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

const state = {
    wordsbyuser:""
}
const mutations = {
    ['SET_WORDS'] (state, text) {
        state.wordsbyuser = text;
    }
}

export default new Vuex.Store({
    state,
    mutations,
    strict: true
})
