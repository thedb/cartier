<?php
/**
 * Created by PhpStorm.
 * User: xinhuang1
 * Date: 2016-11-14
 * Time: 13:34
 */

header('content-type:application/json;charset=utf-8');

$shareUrl = trim($_SERVER['HTTP_REFERER']);

if(strlen($shareUrl) == 0) {
    echo json_encode(array('result' => 0));

    exit();
}

$result = post_data($shareUrl);

if(!is_array($result) || !array_key_exists('appId', $result)){
    echo json_encode(array('result' => 0));

    exit();
}

echo json_encode(array(
    'result' => 1,
    'appId' => $result['appId'],
    'timestamp' => $result['timestamp'],
    'nonce' => $result['nonce'],
    'signature' => $result['signature'],
));

exit();

function post_data($shareUrl)
{
    $data = array(
        'brandCode' => 'CA',
        'campaignName' => 'invitation',
        'url' => $shareUrl,
    );

    $jsonData = json_encode($data);
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, 'https://wechat-framework-preprod.intranet.rccad.net/JSSDK/init');
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    //curl_setopt($ch, CURLOPT_SSL_CIPHER_LIST, CURL_SSLVERSION_TLSv1_2);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_USERPWD, 'CAUser:Richemont');
    curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonData);
    curl_setopt($ch, CURLOPT_TIMEOUT, 10);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json;charset=UTF-8',
            'Content-Length: ' . strlen($jsonData))
    );

    $response = curl_exec($ch);

    error_log(curl_error($ch));
    curl_close($ch);
    echo $response;
  
    return json_decode($response, true);
}
